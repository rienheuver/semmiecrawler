@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Result (&euro;{{ $currentResult }})</h2>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#weeklyResult" aria-controls="weeklyResult" role="tab" data-toggle="tab">Weekly</a></li>
                    <li><a href="#monthlyResult" aria-controls="monthlyResult" role="tab" data-toggle="tab">Monthly</a></li>
                    <li><a href="#allTimeResult" aria-controls="allTimeResult" role="tab" data-toggle="tab">All time</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="weeklyResult">
                        <div id="weekly-result-chart"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="monthlyResult">
                        <div id="monthly-result-chart"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="allTimeResult">
                        <div id="all-time-result-chart"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h2>Yield ({{ $currentYield }}%)</h2>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#weeklyYield" aria-controls="weeklyYield" role="tab" data-toggle="tab">Weekly</a></li>
                    <li><a href="#monthlyYield" aria-controls="monthlyYield" role="tab" data-toggle="tab">Monthly</a></li>
                    <li><a href="#allTimeYield" aria-controls="allTimeYield" role="tab" data-toggle="tab">All time</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="weeklyYield">
                        <div id="weekly-yield-chart"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="monthlyYield">
                        <div id="monthly-yield-chart"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="allTimeYield">
                        <div id="all-time-yield-chart"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h2>Balance (&euro;{{ $currentBalance }})</h2>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#weeklyBalance" aria-controls="weeklyBalance" role="tab" data-toggle="tab">Weekly</a></li>
                    <li><a href="#monthlyBalance" aria-controls="monthlyBalance" role="tab" data-toggle="tab">Monthly</a></li>
                    <li><a href="#allTimeBalance" aria-controls="allTimeBalance" role="tab" data-toggle="tab">All time</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="weeklyBalance">
                        <div id="weekly-balance-chart"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="monthlyBalance">
                        <div id="monthly-balance-chart"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="allTimeBalance">
                        <div id="all-time-balance-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        google.charts.load('current', {'packages':['line', 'corechart']});
        google.charts.setOnLoadCallback(runCharts);

        let resultDataAllTime;
        let resultDataWeekly;
        let resultDataMonthly;
        let yieldDataAllTime;
        let yieldDataWeekly;
        let yieldDataMonthly;
        let balanceDataAllTime;
        let balanceDataWeekly;
        let balanceDataMonthly;

        let resultChartAllTime;
        let resultChartWeekly;
        let resultChartMonthly;
        let yieldChartAllTime;
        let yieldChartWeekly;
        let yieldChartMonthly;
        let balanceChartAllTime;
        let balanceChartWeekly;
        let balanceChartMonthly;

        function runCharts() {
            initializeCharts();
            drawCharts();
        }

        function drawCharts() {
            drawResultAllTime();
            drawResultWeekly();
            drawResultMonthly();
            drawYieldAllTime();
            drawYieldWeekly();
            drawYieldMonthly();
            drawBalanceAllTime();
            drawBalanceWeekly();
            drawBalanceMonthly();
        }

        function initializeCharts() {
            initializeResultAllTime();
            initializeResultWeekly();
            initializeResultMonthly();
            initializeYieldAllTime();
            initializeYieldWeekly();
            initializeYieldMonthly();
            initializeBalanceAllTime();
            initializeBalanceWeekly();
            initializeBalanceMonthly();
        }

        function initializeResultAllTime() {
            resultDataAllTime = new google.visualization.DataTable();

            resultDataAllTime.addColumn('date', 'Date');
            resultDataAllTime.addColumn('number', "Euro's");

            resultDataAllTime.addRows([
                {!! $resultDataAllTime !!}
            ]);

            resultChartAllTime = new google.visualization.LineChart(document.getElementById('all-time-result-chart'));
        }

        function initializeResultWeekly() {
            resultDataWeekly = new google.visualization.DataTable();

            resultDataWeekly.addColumn('date', 'Date');
            resultDataWeekly.addColumn('number', "Euro's");

            resultDataWeekly.addRows([
                {!! $resultDataWeekly !!}
            ]);

            resultChartWeekly = new google.visualization.LineChart(document.getElementById('weekly-result-chart'));
        }

        function initializeResultMonthly() {
            resultDataMonthly = new google.visualization.DataTable();

            resultDataMonthly.addColumn('date', 'Date');
            resultDataMonthly.addColumn('number', "Euro's");

            resultDataMonthly.addRows([
                {!! $resultDataMonthly !!}
            ]);

            resultChartMonthly = new google.visualization.LineChart(document.getElementById('monthly-result-chart'));
        }

        function initializeYieldAllTime() {
            yieldDataAllTime = new google.visualization.DataTable();

            yieldDataAllTime.addColumn('date', 'Date');
            yieldDataAllTime.addColumn('number', "Euro's");

            yieldDataAllTime.addRows([
                {!! $yieldDataAllTime !!}
            ]);

            yieldChartAllTime = new google.visualization.LineChart(document.getElementById('all-time-yield-chart'));
        }

        function initializeYieldWeekly() {
            yieldDataWeekly = new google.visualization.DataTable();

            yieldDataWeekly.addColumn('date', 'Date');
            yieldDataWeekly.addColumn('number', "Euro's");

            yieldDataWeekly.addRows([
                {!! $yieldDataWeekly !!}
            ]);

            yieldChartWeekly = new google.visualization.LineChart(document.getElementById('weekly-yield-chart'));
        }

        function initializeYieldMonthly() {
            yieldDataMonthly = new google.visualization.DataTable();

            yieldDataMonthly.addColumn('date', 'Date');
            yieldDataMonthly.addColumn('number', "Euro's");

            yieldDataMonthly.addRows([
                {!! $yieldDataMonthly !!}
            ]);

            yieldChartMonthly = new google.visualization.LineChart(document.getElementById('monthly-yield-chart'));
        }

        function initializeBalanceAllTime() {
            balanceDataAllTime = new google.visualization.DataTable();

            balanceDataAllTime.addColumn('date', 'Date');
            balanceDataAllTime.addColumn('number', "Euro's");

            balanceDataAllTime.addRows([
                {!! $balanceDataAllTime !!}
            ]);

            balanceChartAllTime = new google.visualization.LineChart(document.getElementById('all-time-balance-chart'));
        }

        function initializeBalanceWeekly() {
            balanceDataWeekly = new google.visualization.DataTable();

            balanceDataWeekly.addColumn('date', 'Date');
            balanceDataWeekly.addColumn('number', "Euro's");

            balanceDataWeekly.addRows([
                {!! $balanceDataWeekly !!}
            ]);

            balanceChartWeekly = new google.visualization.LineChart(document.getElementById('weekly-balance-chart'));
        }

        function initializeBalanceMonthly() {
            balanceDataMonthly = new google.visualization.DataTable();

            balanceDataMonthly.addColumn('date', 'Date');
            balanceDataMonthly.addColumn('number', "Euro's");

            balanceDataMonthly.addRows([
                {!! $balanceDataMonthly !!}
            ]);

            balanceChartMonthly = new google.visualization.LineChart(document.getElementById('monthly-balance-chart'));
        }

        function defaultOptions() {
            return {
                title: 'Semmie Data',
                explorer: {
                    keepInBounds: true,
                    maxZoomOut: 1,
                    zoomDelta: 1.1,
                },
                backgroundColor: {
                    fill: 'transparent',
                },
                animation: {
                    startup: true,
                    duration: 1000,
                    easing: 'out',
                }
            };
        }

        function drawResultAllTime() {
            let options = defaultOptions();
            options.title = "Semmie Result";
            options.colors = ['#e1bc29'];
            resultChartAllTime.draw(resultDataAllTime, options);
        }

        function drawResultWeekly() {
            let options = defaultOptions();
            options.title = "Semmie Result";
            options.colors = ['#e1bc29'];
            resultChartWeekly.draw(resultDataWeekly, options);
        }

        function drawResultMonthly() {
            let options = defaultOptions();
            options.title = "Semmie Result";
            options.colors = ['#e1bc29'];
            resultChartMonthly.draw(resultDataMonthly, options);
        }

        function drawYieldAllTime() {
            let options = defaultOptions();
            options.title = "Semmie Yield";
            options.colors = ['#35a7ff'];
            yieldChartAllTime.draw(yieldDataAllTime, options);
        }

        function drawYieldWeekly() {
            let options = defaultOptions();
            options.title = "Semmie Yield";
            options.colors = ['#35a7ff'];
            yieldChartWeekly.draw(yieldDataWeekly, options);
        }

        function drawYieldMonthly() {
            let options = defaultOptions();
            options.title = "Semmie Yield";
            options.colors = ['#35a7ff'];
            yieldChartMonthly.draw(yieldDataMonthly, options);
        }

        function drawBalanceAllTime() {
            let options = defaultOptions();
            options.title = "Semmie Balance";
            options.colors = ['#13c4a3'];
            balanceChartAllTime.draw(balanceDataAllTime, options);
        }

        function drawBalanceWeekly() {
            let options = defaultOptions();
            options.title = "Semmie Balance";
            options.colors = ['#13c4a3'];
            balanceChartWeekly.draw(balanceDataWeekly, options);
        }

        function drawBalanceMonthly() {
            let options = defaultOptions();
            options.title = "Semmie Balance";
            options.colors = ['#13c4a3'];
            balanceChartMonthly.draw(balanceDataMonthly, options);
        }

        $(window).resize(function(){
            drawCharts();
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            console.log("Change");
            drawCharts();
        })
    </script>
@endsection
