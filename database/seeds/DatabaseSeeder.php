<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SemmieCrawler\Models\User::class)->create([
            'name' => 'Rien',
            'email' => 'rienheuver@gmail.com',
            'password' => bcrypt('wachtwoord'),
        ]);
    }
}
