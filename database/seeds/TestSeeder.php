<?php

use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 60; $i++) {
            factory(SemmieCrawler\Models\Statistic::class)->create([
                'created_at' => \Carbon\Carbon::now()->subDay($i),
                'updated_at' => \Carbon\Carbon::now()->subDay($i),
            ]);
        }
    }
}
