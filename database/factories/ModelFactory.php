<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(SemmieCrawler\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(SemmieCrawler\Models\Statistic::class, function (Faker\Generator $faker) {
    $yield = $faker->numberBetween(-200,500)/10000;
    $costs = $faker->numberBetween(2,7);
    $result = 3000*$yield-$costs;
    $balance = 3000+$result;

    return [
        'balance' => $balance,
        'yield' => $yield,
        'result' => $result
    ];
});
