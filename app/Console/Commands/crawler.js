const puppeteer = require('puppeteer');
const credentials = require('./.env');

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
  });
  const page = await browser.newPage();
  await page.goto('https://mijn.semmie.nl', { waitUntil: 'networkidle2' });
  await page.waitForSelector('input[type="email"]');

  await page.focus('input[type="email"]');
  await page.keyboard.type(credentials.username);

  await page.focus('input[type="password"]');
  await page.keyboard.type(credentials.password);

  await page.click('.login__form .button');

  await page.waitForNavigation();

  await page.goto(
    'https://mijn.semmie.nl/web/accounts/1c246342-a47b-4e6e-b266-de3fdd449835',
    { waitUntil: 'networkidle2' }
  );

  await page.waitForSelector('.profit');


  const data = await page.evaluate(() => {
    let data = {};
    const cards = document.querySelectorAll('.web-card');
    const balanceBlock = cards[1].querySelectorAll(
      '.gridlayout.first .value-container label'
    );
    const balance = parseFloat(
      balanceBlock[0].innerText.replace(/[.€]/g, '').replace(/,/g, '.')
    );
    data.balance = balance;

    const resultBlock = cards[1].querySelector(
      '.gridlayout.last .value-container span'
    );
    const result = parseFloat(
      resultBlock.innerText.replace(/[.€]/g, '').replace(/,/g, '.')
    );
    data.result = result;

    const yieldBlock = cards[2].querySelectorAll(
      '.gridlayout.first .value-container .value-container label'
    );
    const yield = parseFloat(
      yieldBlock[0].innerText.replace(/[.%]/g, '').replace(/,/g, '.')
    );
    data.yield = yield;

    return data;
  });

  console.log(JSON.stringify(data));

  browser.close();
})();
