<?php

namespace SemmieCrawler\Http\Controllers;

use Carbon\Carbon;
use SemmieCrawler\Models\Statistic;
use NumberFormatter;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stats = Statistic::orderBy('created_at', 'asc')->get();

        $formatter = new NumberFormatter('en_GB', NumberFormatter::DECIMAL);
        $formatter->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS, 2);
        $formatter->setSymbol(NumberFormatter::GROUPING_SEPARATOR_SYMBOL, "");

        $labels = $stats->map(function ($stat) {
            return $stat->created_at->format('d-m-Y');
        });

        $balanceDataAllTime = substr($stats->reduce(function ($string, $stat) use ($formatter) {
            $date = $stat->created_at->timestamp * 1000;
            return $string . ", [new Date(" . $date . "), " . $formatter->format($stat->balance) . "]";
        }), 2);
        $balanceDataWeekly = substr($stats->reduce(function ($string, $stat) use ($formatter) {
            $today = Carbon::now()->subWeek();
            if ($stat->created_at->gte($today)) {
                $date = $stat->created_at->timestamp * 1000;
                return $string . ", [new Date(" . $date . "), " . $formatter->format($stat->balance) . "]";
            } else {
                return '';
            }
        }), 2);
        $balanceDataMonthly = substr($stats->reduce(function ($string, $stat) use ($formatter) {
            $today = Carbon::now()->subMonth();
            if ($stat->created_at->gte($today)) {
                $date = $stat->created_at->timestamp * 1000;
                return $string . ", [new Date(" . $date . "), " . $formatter->format($stat->balance) . "]";
            } else {
                return '';
            }
        }), 2);

        $yieldDataAllTime = substr($stats->reduce(function ($string, $stat) {
            $date = $stat->created_at->timestamp * 1000;
            return $string . ", [new Date(" . $date . "), " . $stat->yield * 100 . "]";
        }), 2);
        $yieldDataWeekly = substr($stats->reduce(function ($string, $stat) {
            $today = Carbon::now()->subWeek();
            if ($stat->created_at->gte($today)) {
                $date = $stat->created_at->timestamp * 1000;
                return $string . ", [new Date(" . $date . "), " . $stat->yield * 100 . "]";
            } else {
                return '';
            }
        }), 2);
        $yieldDataMonthly = substr($stats->reduce(function ($string, $stat) {
            $today = Carbon::now()->subMonth();
            if ($stat->created_at->gte($today)) {
                $date = $stat->created_at->timestamp * 1000;
                return $string . ", [new Date(" . $date . "), " . $stat->yield * 100 . "]";
            } else {
                return '';
            }
        }), 2);

        $resultDataAllTime = substr($stats->reduce(function ($string, $stat) use ($formatter) {
            $date = $stat->created_at->timestamp * 1000;
            return $string . ", [new Date(" . $date . "), " . $formatter->format($stat->result) . "]";
        }), 2);
        $resultDataWeekly = substr($stats->reduce(function ($string, $stat) use ($formatter) {
            $today = Carbon::now()->subWeek();
            if ($stat->created_at->gte($today)) {
                $date = $stat->created_at->timestamp * 1000;
                return $string . ", [new Date(" . $date . "), " . $formatter->format($stat->result) . "]";
            } else {
                return '';
            }
        }), 2);
        $resultDataMonthly = substr($stats->reduce(function ($string, $stat) use ($formatter) {
            $today = Carbon::now()->subMonth();
            if ($stat->created_at->gte($today)) {
                $date = $stat->created_at->timestamp * 1000;
                return $string . ", [new Date(" . $date . "), " . $formatter->format($stat->result) . "]";
            } else {
                return '';
            }
        }), 2);

        $formatter = new NumberFormatter('en_GB', NumberFormatter::CURRENCY);
        $formatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
        return view('home')->with([
            'currentBalance'     => $formatter->format($stats->last()->balance),
            'currentYield'       => $stats->last()->yield * 100,
            'currentResult'      => $formatter->format($stats->last()->result),
            'labels'             => json_encode($labels),
            'balanceDataAllTime' => $balanceDataAllTime,
            'balanceDataWeekly'  => $balanceDataWeekly,
            'balanceDataMonthly' => $balanceDataMonthly,
            'yieldDataAllTime'   => $yieldDataAllTime,
            'yieldDataWeekly'    => $yieldDataWeekly,
            'yieldDataMonthly'   => $yieldDataMonthly,
            'resultDataAllTime'  => $resultDataAllTime,
            'resultDataWeekly'   => $resultDataWeekly,
            'resultDataMonthly'  => $resultDataMonthly,
        ]);
    }
}
