<?php

namespace SemmieCrawler\Models;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $fillable = ['balance','yield','result'];

    protected $dates = ['created_at', 'updated_at'];
    
}
