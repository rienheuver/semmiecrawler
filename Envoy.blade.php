@servers(['web' => 'deployer@rienheuver.nl'])

@setup
$repository = 'git@gitlab.com:rienheuver/semmiecrawler.git';
$releases_dir = '/var/www/data/' . $target. '/semmiecrawler/releases';
$app_dir = '/var/www/data/' . $target. '/semmiecrawler';
$release = date('YmdHis');
$new_release_dir = $releases_dir .'/'. $release;
$old_release_dir = $releases_dir .'/'. scandir($releases_dir, 1)[0];
@endsetup

@story('deploy')
clone_repository
run_composer
app_down
update_symlinks
app_up
@endstory

@task('clone_repository')
echo "Cloning repository"
[ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir }}
composer install --prefer-dist --no-scripts -q -o
npm --prefix ./app/Console/Commands install ./app/Console/Commands
@endtask

@task('update_symlinks')
file_count_old=(`ls -l {{ $app_dir }}/current/database/migrations | wc -l`)
file_count_new=(`ls -l {{ $new_release_dir }}/database/migrations | wc -l`)

if [ $file_count_old -gt $file_count_new ]; then
echo "Performing a rollback"
cd {{ $app_dir }}/current
php artisan migrate:rollback --force
fi

echo "Updating permissions"
chown deployer:www-data -R {{ $new_release_dir }}/bootstrap/cache
chown deployer:www-data -R {{ $new_release_dir }}/storage/logs

echo "Linking storage directory"
rm -rf {{ $new_release_dir }}/storage
ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

echo 'Linking .env.js file'
ln -nfs {{ $app_dir }}/.env.js {{ $new_release_dir }}/app/Console/Commands/.env.js

echo 'Linking current release'
ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current

if [ $file_count_old -le $file_count_new ]; then
echo "Performing a migrate"
cd {{ $app_dir }}/current
php artisan migrate --force
fi

{{-- Recursive remove -> list only directories (default rightly sorted) -> take first from list --}}
echo "Removing directory $(ls -d {{ $releases_dir }}/*/ | head -n1)"
rm -rf "$(ls -d {{ $releases_dir }}/*/ | head -n1)"

@endtask

@task('app_up')
echo "Application up"
cd {{ $app_dir }}/current
php artisan up
@endtask

@task('app_down')
echo "Application down"
cd {{ $app_dir }}/current
php artisan down
php artisan config:clear
@endtask

